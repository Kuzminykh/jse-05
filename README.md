### Описание приложения task-manager
***
Учебный проект в рамках курса Java!!!

#### Требования к Hardware
1. CPU i5 / i7
2. RAM 16GB (8gb )
3. LAN / WI-FI
4. SSD 40GB (НDD)

#### Требование к Software: 
1. Java 11.0. 
2. Apache Maven 3.6.3
3. Java Open JDK 11

#### Стек технологий:
1. Java Open JDK 11
2. Apache Maven 3.6.3
3. Intellij Idea 2020.1.2
4. Git 2.27.0

#### Разработчик: 
 Кузьминых Н.Н.
 
#### e-mail: 
 kuzminykh_nn@nlmk.com

#### Сборка приложения: 
- `mvn clean` - Удаление всех созданных в процессе сборки артефактов
- `mvn package` - Создание пакета
- `mvn install` - Копирование .jar в удаленный репозиторий

#### Команда для запуска приложения: 
``` 
java -jar target/task-manager-1.0.0.jar 
```

*Дополнительные параметры запуска (териминальные команды):*
```
 * about - Информация о разработчике
 * version - Версия программы
 * help - Отображение списка терминальных команд
 * exit - Выход из приложения
```

 `Настроена передача изменений по протоколу SSH в GIT`
 